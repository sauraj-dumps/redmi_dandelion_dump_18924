## dandelion-user 11 RP1A.200720.011 V12.5.5.0.RCZMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6765
- Codename: dandelion
- Brand: Redmi
- Flavor: dandelion-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.5.5.0.RCZMIXM
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/dandelion_global2/dandelion:11/RP1A.200720.011/V12.5.5.0.RCZMIXM:user/release-keys
- OTA version: 
- Branch: dandelion-user-11-RP1A.200720.011-V12.5.5.0.RCZMIXM-release-keys
- Repo: redmi_dandelion_dump_18924


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
